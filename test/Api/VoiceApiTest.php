<?php
/**
 * VoiceApiTest
 * PHP version 5
 *
 * @category Class
 * @package  Braango
 * @author   Swagger Codegen team
 * @link     https://github.com/swagger-api/swagger-codegen
 */

/**
 * braango
 *
 * Braango API for partners to onboard dealers , configure and allow for send messages on behalf of dealers
 *
 * OpenAPI spec version: 2.0
 * 
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 *
 */

/**
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen
 * Please update the test case below to test the endpoint.
 */

namespace Braango;

use \Braango\Configuration;
use \Braango\ApiClient;
use \Braango\ApiException;
use \Braango\ObjectSerializer;

/**
 * VoiceApiTest Class Doc Comment
 *
 * @category Class
 * @package  Braango
 * @author   Swagger Codegen team
 * @link     https://github.com/swagger-api/swagger-codegen
 */
class VoiceApiTest extends \PHPUnit_Framework_TestCase
{

    /**
     * Setup before running any test cases
     */
    public static function setUpBeforeClass()
    {
    }

    /**
     * Setup before running each test case
     */
    public function setUp()
    {
    }

    /**
     * Clean up after running each test case
     */
    public function tearDown()
    {
    }

    /**
     * Clean up after running all test cases
     */
    public static function tearDownAfterClass()
    {
    }

    /**
     * Test case for createVoice
     *
     * Create Voice.
     *
     */
    public function testCreateVoice()
    {
    }

    /**
     * Test case for deleteOneVoice
     *
     * Delete One Voice.
     *
     */
    public function testDeleteOneVoice()
    {
    }

    /**
     * Test case for deleteVoice
     *
     * Delete Voice.
     *
     */
    public function testDeleteVoice()
    {
    }

    /**
     * Test case for getVoice
     *
     * Get Voice.
     *
     */
    public function testGetVoice()
    {
    }
}
