# Braango\BraangonumbersApi

All URIs are relative to *https://api.braango.com/v2/braango*

Method | HTTP request | Description
------------- | ------------- | -------------
[**createBraangoNumber**](BraangonumbersApi.md#createBraangoNumber) | **POST** /braangonumber/{subdealerid}/{group} | Create braangonumber
[**deleteBraangoNumber**](BraangonumbersApi.md#deleteBraangoNumber) | **DELETE** /braangonumber/{subdealerid}/{braangonumber_or_uuid} | Delete braangonumber
[**getBraangoNumber**](BraangonumbersApi.md#getBraangoNumber) | **GET** /braangonumber/{subdealerid}/{braangonumber_or_uuid} | Get braangonumber
[**getBraangoNumbers**](BraangonumbersApi.md#getBraangoNumbers) | **GET** /braangonumber/{subdealerid} | List braangonumbers
[**updateBraangoNumber**](BraangonumbersApi.md#updateBraangoNumber) | **PUT** /braangonumber/{subdealerid}/{braangonumber_or_uuid} | Update braangonumber


# **createBraangoNumber**
> \Braango\braangomodel\BraangoNumberOutput createBraangoNumber($subdealerid, $group, $body)

Create braangonumber

Create Braango Number for a given group. Either all personnel associated with the group can be added to the braango number or selected few

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: auth_token
Braango\Configuration::getDefaultConfiguration()->setApiKey('auth_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Braango\Configuration::getDefaultConfiguration()->setApiKeyPrefix('auth_token', 'Bearer');

$api_instance = new Braango\Api\BraangonumbersApi();
$subdealerid = "subdealerid_example"; // string | 
$group = "group_example"; // string | 
$body = new \Braango\braangomodel\BraangoNumberCreateInput(); // \Braango\braangomodel\BraangoNumberCreateInput | 

try {
    $result = $api_instance->createBraangoNumber($subdealerid, $group, $body);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling BraangonumbersApi->createBraangoNumber: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **subdealerid** | **string**|  |
 **group** | **string**|  |
 **body** | [**\Braango\braangomodel\BraangoNumberCreateInput**](../Model/BraangoNumberCreateInput.md)|  | [optional]

### Return type

[**\Braango\braangomodel\BraangoNumberOutput**](../Model/BraangoNumberOutput.md)

### Authorization

[auth_token](../../README.md#auth_token)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **deleteBraangoNumber**
> object deleteBraangoNumber($subdealerid, $braangonumberOrUuid, $fakeBraangoNumber, $apiKey, $accountType)

Delete braangonumber

Delete a given braango number. The number will be released and cannot be used anywhere. This is irrervisble action

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: auth_token
Braango\Configuration::getDefaultConfiguration()->setApiKey('auth_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Braango\Configuration::getDefaultConfiguration()->setApiKeyPrefix('auth_token', 'Bearer');

$api_instance = new Braango\Api\BraangonumbersApi();
$subdealerid = "subdealerid_example"; // string | 
$braangonumberOrUuid = "braangonumberOrUuid_example"; // string | 
$fakeBraangoNumber = true; // bool | 
$apiKey = "<<api_key>>"; // string | API Key to access this dealer's resources. Value was returned when create_account api was called and dealer was created first time
$accountType = "<<account_type>>"; // string | Dealer or partner is accessing this API

try {
    $result = $api_instance->deleteBraangoNumber($subdealerid, $braangonumberOrUuid, $fakeBraangoNumber, $apiKey, $accountType);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling BraangonumbersApi->deleteBraangoNumber: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **subdealerid** | **string**|  |
 **braangonumberOrUuid** | **string**|  |
 **fakeBraangoNumber** | **bool**|  |
 **apiKey** | **string**| API Key to access this dealer&#39;s resources. Value was returned when create_account api was called and dealer was created first time | [default to &lt;&lt;api_key&gt;&gt;]
 **accountType** | **string**| Dealer or partner is accessing this API | [default to &lt;&lt;account_type&gt;&gt;]

### Return type

**object**

### Authorization

[auth_token](../../README.md#auth_token)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getBraangoNumber**
> \Braango\braangomodel\BraangoNumberOutput getBraangoNumber($subdealerid, $braangonumberOrUuid, $apiKey, $accountType)

Get braangonumber

Get the braango number object as specified either by braango number or UUID of the braango number

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: auth_token
Braango\Configuration::getDefaultConfiguration()->setApiKey('auth_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Braango\Configuration::getDefaultConfiguration()->setApiKeyPrefix('auth_token', 'Bearer');

$api_instance = new Braango\Api\BraangonumbersApi();
$subdealerid = "subdealerid_example"; // string | 
$braangonumberOrUuid = "braangonumberOrUuid_example"; // string | 
$apiKey = "<<api_key>>"; // string | API Key to access this dealer's resources. Value was returned when create_account api was called and dealer was created first time
$accountType = "<<account_type>>"; // string | Dealer or partner is accessing this API

try {
    $result = $api_instance->getBraangoNumber($subdealerid, $braangonumberOrUuid, $apiKey, $accountType);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling BraangonumbersApi->getBraangoNumber: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **subdealerid** | **string**|  |
 **braangonumberOrUuid** | **string**|  |
 **apiKey** | **string**| API Key to access this dealer&#39;s resources. Value was returned when create_account api was called and dealer was created first time | [default to &lt;&lt;api_key&gt;&gt;]
 **accountType** | **string**| Dealer or partner is accessing this API | [default to &lt;&lt;account_type&gt;&gt;]

### Return type

[**\Braango\braangomodel\BraangoNumberOutput**](../Model/BraangoNumberOutput.md)

### Authorization

[auth_token](../../README.md#auth_token)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getBraangoNumbers**
> \Braango\braangomodel\BraangoNumberOutput getBraangoNumbers($subdealerid, $apiKey, $accountType)

List braangonumbers

List all the braango numbers for given _sub_dealer_

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: auth_token
Braango\Configuration::getDefaultConfiguration()->setApiKey('auth_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Braango\Configuration::getDefaultConfiguration()->setApiKeyPrefix('auth_token', 'Bearer');

$api_instance = new Braango\Api\BraangonumbersApi();
$subdealerid = "subdealerid_example"; // string | 
$apiKey = "<<api_key>>"; // string | API Key to access this dealer's resources. Value was returned when create_account api was called and dealer was created first time
$accountType = "<<account_type>>"; // string | Dealer or partner is accessing this API

try {
    $result = $api_instance->getBraangoNumbers($subdealerid, $apiKey, $accountType);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling BraangonumbersApi->getBraangoNumbers: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **subdealerid** | **string**|  |
 **apiKey** | **string**| API Key to access this dealer&#39;s resources. Value was returned when create_account api was called and dealer was created first time | [default to &lt;&lt;api_key&gt;&gt;]
 **accountType** | **string**| Dealer or partner is accessing this API | [default to &lt;&lt;account_type&gt;&gt;]

### Return type

[**\Braango\braangomodel\BraangoNumberOutput**](../Model/BraangoNumberOutput.md)

### Authorization

[auth_token](../../README.md#auth_token)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **updateBraangoNumber**
> \Braango\braangomodel\BraangoNumberOutput updateBraangoNumber($subdealerid, $braangonumberOrUuid, $body)

Update braangonumber

Update the _braango_number_ as specified by UUID or actual number in URL. Either the personnel could be added or deleted as specified by add flag

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: auth_token
Braango\Configuration::getDefaultConfiguration()->setApiKey('auth_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Braango\Configuration::getDefaultConfiguration()->setApiKeyPrefix('auth_token', 'Bearer');

$api_instance = new Braango\Api\BraangonumbersApi();
$subdealerid = "subdealerid_example"; // string | 
$braangonumberOrUuid = "braangonumberOrUuid_example"; // string | 
$body = new \Braango\braangomodel\BraangoNumberInput(); // \Braango\braangomodel\BraangoNumberInput | 

try {
    $result = $api_instance->updateBraangoNumber($subdealerid, $braangonumberOrUuid, $body);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling BraangonumbersApi->updateBraangoNumber: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **subdealerid** | **string**|  |
 **braangonumberOrUuid** | **string**|  |
 **body** | [**\Braango\braangomodel\BraangoNumberInput**](../Model/BraangoNumberInput.md)|  | [optional]

### Return type

[**\Braango\braangomodel\BraangoNumberOutput**](../Model/BraangoNumberOutput.md)

### Authorization

[auth_token](../../README.md#auth_token)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

