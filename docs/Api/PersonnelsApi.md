# Braango\PersonnelsApi

All URIs are relative to *https://api.braango.com/v2/braango*

Method | HTTP request | Description
------------- | ------------- | -------------
[**createPersonnel**](PersonnelsApi.md#createPersonnel) | **POST** /personnel/{subdealerid} | Create personnel regular
[**createSubDealer**](PersonnelsApi.md#createSubDealer) | **POST** /personnel | Create SubDealer
[**deletePersonnel**](PersonnelsApi.md#deletePersonnel) | **DELETE** /personnel/{subdealerid}/{salespersonid} | Delete personnel
[**getAllPersonnelForDealer**](PersonnelsApi.md#getAllPersonnelForDealer) | **GET** /personnel | List all personnels
[**getPersonnel**](PersonnelsApi.md#getPersonnel) | **GET** /personnel/{subdealerid}/{salespersonid} | Get personnel
[**getPersonnelsPerSubDealer**](PersonnelsApi.md#getPersonnelsPerSubDealer) | **GET** /personnel/{subdealerid} | List personnels
[**updatePersonnel**](PersonnelsApi.md#updatePersonnel) | **PUT** /personnel/{subdealerid}/{salespersonid} | Update personnel


# **createPersonnel**
> \Braango\braangomodel\PersonnelOutputWrapper createPersonnel($subdealerid, $body)

Create personnel regular

This api call creates the personnel for the current _sub_dealer_.  Personnel is the master resource in braango system. It consists of following sub-resources   * SMS * VOICE * EMAIL/CRM EMAIL * BANNERS * FOOTERS * GROUPS * SUPERVISORS * CRM OBJECT  * WEB HOOK  Either the consumer of this API can create and update entire _personnel_ object or create bits and pieces and update the _sub_resources_ as an when needed. For the benefit of developer, majority of _sub_resouces_ of _personnel_ entity are exposed via API

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: auth_token
Braango\Configuration::getDefaultConfiguration()->setApiKey('auth_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Braango\Configuration::getDefaultConfiguration()->setApiKeyPrefix('auth_token', 'Bearer');

$api_instance = new Braango\Api\PersonnelsApi();
$subdealerid = "subdealerid_example"; // string | 
$body = new \Braango\braangomodel\PersonnelRequestInput(); // \Braango\braangomodel\PersonnelRequestInput | 

try {
    $result = $api_instance->createPersonnel($subdealerid, $body);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling PersonnelsApi->createPersonnel: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **subdealerid** | **string**|  |
 **body** | [**\Braango\braangomodel\PersonnelRequestInput**](../Model/PersonnelRequestInput.md)|  | [optional]

### Return type

[**\Braango\braangomodel\PersonnelOutputWrapper**](../Model/PersonnelOutputWrapper.md)

### Authorization

[auth_token](../../README.md#auth_token)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **createSubDealer**
> \Braango\braangomodel\PersonnelOutputWrapper createSubDealer($body)

Create SubDealer

This api call creates the personnel for the current dealer. This call creates sub_dealer_master_personnel i.e. sub dealer master account. This account typically is assigned to Manager of the location or can be abstract entity. However this account has privileges to create groups that other accounts can subscribe too.   Personnel is the master resource in braango system. It consists of following sub-resources   * SMS * VOICE * EMAIL/CRM EMAIL * BANNERS * FOOTERS * GROUPS * SUPERVISORS * CRM OBJECT   Either the consumer of this API can create and update entire _personnel_ object or create bits and pieces and update the _sub_resources_ as an when needed. For the benefit of developer, majority of _sub_resouces_ of _personnel_ entity are exposed via API

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: auth_token
Braango\Configuration::getDefaultConfiguration()->setApiKey('auth_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Braango\Configuration::getDefaultConfiguration()->setApiKeyPrefix('auth_token', 'Bearer');

$api_instance = new Braango\Api\PersonnelsApi();
$body = new \Braango\braangomodel\SubDealerRequestInput(); // \Braango\braangomodel\SubDealerRequestInput | 

try {
    $result = $api_instance->createSubDealer($body);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling PersonnelsApi->createSubDealer: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**\Braango\braangomodel\SubDealerRequestInput**](../Model/SubDealerRequestInput.md)|  | [optional]

### Return type

[**\Braango\braangomodel\PersonnelOutputWrapper**](../Model/PersonnelOutputWrapper.md)

### Authorization

[auth_token](../../README.md#auth_token)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **deletePersonnel**
> \Braango\braangomodel\PersonnelOutputWrapper deletePersonnel($subdealerid, $salespersonid, $apiKey, $accountType)

Delete personnel

This api call deletes the personnel that was created under subDealer represented by _subdealerid_.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: auth_token
Braango\Configuration::getDefaultConfiguration()->setApiKey('auth_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Braango\Configuration::getDefaultConfiguration()->setApiKeyPrefix('auth_token', 'Bearer');

$api_instance = new Braango\Api\PersonnelsApi();
$subdealerid = "subdealerid_example"; // string | Sub dealer for which this sales person belongs to.
$salespersonid = "salespersonid_example"; // string | Sales person ID that was returned when this personnel was created
$apiKey = "<<api_key>>"; // string | API Key to access this dealer's resources. Value was returned when create_account api was called and dealer was created first time
$accountType = "<<account_type>>"; // string | Dealer or partner is accessing this API

try {
    $result = $api_instance->deletePersonnel($subdealerid, $salespersonid, $apiKey, $accountType);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling PersonnelsApi->deletePersonnel: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **subdealerid** | **string**| Sub dealer for which this sales person belongs to. |
 **salespersonid** | **string**| Sales person ID that was returned when this personnel was created |
 **apiKey** | **string**| API Key to access this dealer&#39;s resources. Value was returned when create_account api was called and dealer was created first time | [default to &lt;&lt;api_key&gt;&gt;]
 **accountType** | **string**| Dealer or partner is accessing this API | [default to &lt;&lt;account_type&gt;&gt;]

### Return type

[**\Braango\braangomodel\PersonnelOutputWrapper**](../Model/PersonnelOutputWrapper.md)

### Authorization

[auth_token](../../README.md#auth_token)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getAllPersonnelForDealer**
> \Braango\braangomodel\PersonnelOutputListAllSubDealersWrapper getAllPersonnelForDealer($apiKey, $accountType)

List all personnels

This api call returns the all personnel that were created under this dealer being accessed via dealer api_key. For virtual dealer model, this will be partner's api_key (different from auth_token though) and this call will list all the sub-dealers (accounts) and their personnel

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: auth_token
Braango\Configuration::getDefaultConfiguration()->setApiKey('auth_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Braango\Configuration::getDefaultConfiguration()->setApiKeyPrefix('auth_token', 'Bearer');

$api_instance = new Braango\Api\PersonnelsApi();
$apiKey = "<<api_key>>"; // string | API Key to access this dealer's resources. Value was returned when create_account api was called and dealer was created first time
$accountType = "<<account_type>>"; // string | Dealer or partner is accessing this API

try {
    $result = $api_instance->getAllPersonnelForDealer($apiKey, $accountType);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling PersonnelsApi->getAllPersonnelForDealer: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **apiKey** | **string**| API Key to access this dealer&#39;s resources. Value was returned when create_account api was called and dealer was created first time | [default to &lt;&lt;api_key&gt;&gt;]
 **accountType** | **string**| Dealer or partner is accessing this API | [default to &lt;&lt;account_type&gt;&gt;]

### Return type

[**\Braango\braangomodel\PersonnelOutputListAllSubDealersWrapper**](../Model/PersonnelOutputListAllSubDealersWrapper.md)

### Authorization

[auth_token](../../README.md#auth_token)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getPersonnel**
> \Braango\braangomodel\PersonnelOutputWrapper getPersonnel($subdealerid, $salespersonid, $apiKey, $accountType)

Get personnel

This api call returns the personnel that was created under subDealer represented by _subdealerid_. The __ID of the personnel__ is __mandaotry__ path parameter.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: auth_token
Braango\Configuration::getDefaultConfiguration()->setApiKey('auth_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Braango\Configuration::getDefaultConfiguration()->setApiKeyPrefix('auth_token', 'Bearer');

$api_instance = new Braango\Api\PersonnelsApi();
$subdealerid = "subdealerid_example"; // string | Sub dealer for which this sales person belongs to.
$salespersonid = "salespersonid_example"; // string | Sales person ID that was returned when this personnel was created
$apiKey = "<<api_key>>"; // string | API Key to access this dealer's resources. Value was returned when create_account api was called and dealer was created first time
$accountType = "<<account_type>>"; // string | Dealer or partner is accessing this API

try {
    $result = $api_instance->getPersonnel($subdealerid, $salespersonid, $apiKey, $accountType);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling PersonnelsApi->getPersonnel: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **subdealerid** | **string**| Sub dealer for which this sales person belongs to. |
 **salespersonid** | **string**| Sales person ID that was returned when this personnel was created |
 **apiKey** | **string**| API Key to access this dealer&#39;s resources. Value was returned when create_account api was called and dealer was created first time | [default to &lt;&lt;api_key&gt;&gt;]
 **accountType** | **string**| Dealer or partner is accessing this API | [default to &lt;&lt;account_type&gt;&gt;]

### Return type

[**\Braango\braangomodel\PersonnelOutputWrapper**](../Model/PersonnelOutputWrapper.md)

### Authorization

[auth_token](../../README.md#auth_token)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getPersonnelsPerSubDealer**
> \Braango\braangomodel\PersonnelOutputListWrapper getPersonnelsPerSubDealer($subdealerid, $apiKey, $accountType)

List personnels

This api call returns the all personnel that were created under subDealer represented by _subdealerid_.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: auth_token
Braango\Configuration::getDefaultConfiguration()->setApiKey('auth_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Braango\Configuration::getDefaultConfiguration()->setApiKeyPrefix('auth_token', 'Bearer');

$api_instance = new Braango\Api\PersonnelsApi();
$subdealerid = "subdealerid_example"; // string | 
$apiKey = "<<api_key>>"; // string | API Key to access this dealer's resources. Value was returned when create_account api was called and dealer was created first time
$accountType = "<<account_type>>"; // string | Dealer or partner is accessing this API

try {
    $result = $api_instance->getPersonnelsPerSubDealer($subdealerid, $apiKey, $accountType);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling PersonnelsApi->getPersonnelsPerSubDealer: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **subdealerid** | **string**|  |
 **apiKey** | **string**| API Key to access this dealer&#39;s resources. Value was returned when create_account api was called and dealer was created first time | [default to &lt;&lt;api_key&gt;&gt;]
 **accountType** | **string**| Dealer or partner is accessing this API | [default to &lt;&lt;account_type&gt;&gt;]

### Return type

[**\Braango\braangomodel\PersonnelOutputListWrapper**](../Model/PersonnelOutputListWrapper.md)

### Authorization

[auth_token](../../README.md#auth_token)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **updatePersonnel**
> \Braango\braangomodel\PersonnelOutputWrapper updatePersonnel($subdealerid, $salespersonid, $body)

Update personnel

This api call is used to update personnel for additions. Practically all the fields of the personnel that are necessary for functioning as braango personnel can be updated to add more except for supervisor. Supervisor needs to be updated,created using a seperate API call    __Note__ _This call will be used to only add additional fields. To actually delete unwanted fields, please use appropiate sub_resource calls_

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: auth_token
Braango\Configuration::getDefaultConfiguration()->setApiKey('auth_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Braango\Configuration::getDefaultConfiguration()->setApiKeyPrefix('auth_token', 'Bearer');

$api_instance = new Braango\Api\PersonnelsApi();
$subdealerid = "subdealerid_example"; // string | Sub dealer for which this sales person belongs to.
$salespersonid = "salespersonid_example"; // string | Sales person ID that was returned when this personnel was created
$body = new \Braango\braangomodel\PersonnelUpdateRequestInput(); // \Braango\braangomodel\PersonnelUpdateRequestInput | 

try {
    $result = $api_instance->updatePersonnel($subdealerid, $salespersonid, $body);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling PersonnelsApi->updatePersonnel: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **subdealerid** | **string**| Sub dealer for which this sales person belongs to. |
 **salespersonid** | **string**| Sales person ID that was returned when this personnel was created |
 **body** | [**\Braango\braangomodel\PersonnelUpdateRequestInput**](../Model/PersonnelUpdateRequestInput.md)|  | [optional]

### Return type

[**\Braango\braangomodel\PersonnelOutputWrapper**](../Model/PersonnelOutputWrapper.md)

### Authorization

[auth_token](../../README.md#auth_token)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

