# Braango\FootersApi

All URIs are relative to *https://api.braango.com/v2/braango*

Method | HTTP request | Description
------------- | ------------- | -------------
[**createFooters**](FootersApi.md#createFooters) | **POST** /personnel/footers/{subdealerid}/{salespersonid} | Create Footers
[**deleteFooters**](FootersApi.md#deleteFooters) | **DELETE** /personnel/footers/{subdealerid}/{salespersonid}/{footertype} | Delete Footers
[**getFooters**](FootersApi.md#getFooters) | **GET** /personnel/footers/{subdealerid}/{salespersonid}/{footertype} | Get Footers


# **createFooters**
> \Braango\braangomodel\FootersOutput createFooters($subdealerid, $salespersonid, $body)

Create Footers

Create list of footers. All three types of footers can be specified here, client, dealer or supervisor

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: auth_token
Braango\Configuration::getDefaultConfiguration()->setApiKey('auth_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Braango\Configuration::getDefaultConfiguration()->setApiKeyPrefix('auth_token', 'Bearer');

$api_instance = new Braango\Api\FootersApi();
$subdealerid = "subdealerid_example"; // string | 
$salespersonid = "salespersonid_example"; // string | 
$body = new \Braango\braangomodel\FootersInput(); // \Braango\braangomodel\FootersInput | 

try {
    $result = $api_instance->createFooters($subdealerid, $salespersonid, $body);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling FootersApi->createFooters: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **subdealerid** | **string**|  |
 **salespersonid** | **string**|  |
 **body** | [**\Braango\braangomodel\FootersInput**](../Model/FootersInput.md)|  | [optional]

### Return type

[**\Braango\braangomodel\FootersOutput**](../Model/FootersOutput.md)

### Authorization

[auth_token](../../README.md#auth_token)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **deleteFooters**
> \Braango\braangomodel\FootersOutput deleteFooters($subdealerid, $salespersonid, $footertype, $apiKey, $accountType)

Delete Footers

Delete footers, footer type needs to be specified in the URL

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: auth_token
Braango\Configuration::getDefaultConfiguration()->setApiKey('auth_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Braango\Configuration::getDefaultConfiguration()->setApiKeyPrefix('auth_token', 'Bearer');

$api_instance = new Braango\Api\FootersApi();
$subdealerid = "subdealerid_example"; // string | 
$salespersonid = "salespersonid_example"; // string | 
$footertype = "footertype_example"; // string | 
$apiKey = "<<api_key>>"; // string | API Key to access this dealer's resources. Value was returned when create_account api was called and dealer was created first time
$accountType = "<<account_type>>"; // string | Dealer or partner is accessing this API

try {
    $result = $api_instance->deleteFooters($subdealerid, $salespersonid, $footertype, $apiKey, $accountType);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling FootersApi->deleteFooters: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **subdealerid** | **string**|  |
 **salespersonid** | **string**|  |
 **footertype** | **string**|  |
 **apiKey** | **string**| API Key to access this dealer&#39;s resources. Value was returned when create_account api was called and dealer was created first time | [default to &lt;&lt;api_key&gt;&gt;]
 **accountType** | **string**| Dealer or partner is accessing this API | [default to &lt;&lt;account_type&gt;&gt;]

### Return type

[**\Braango\braangomodel\FootersOutput**](../Model/FootersOutput.md)

### Authorization

[auth_token](../../README.md#auth_token)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getFooters**
> \Braango\braangomodel\FootersOutput getFooters($subdealerid, $salespersonid, $footertype, $apiKey, $accountType)

Get Footers

Get list of footers. Footer type needs to be specified in the URL

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: auth_token
Braango\Configuration::getDefaultConfiguration()->setApiKey('auth_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Braango\Configuration::getDefaultConfiguration()->setApiKeyPrefix('auth_token', 'Bearer');

$api_instance = new Braango\Api\FootersApi();
$subdealerid = "subdealerid_example"; // string | 
$salespersonid = "salespersonid_example"; // string | 
$footertype = "footertype_example"; // string | 
$apiKey = "<<api_key>>"; // string | API Key to access this dealer's resources. Value was returned when create_account api was called and dealer was created first time
$accountType = "<<account_type>>"; // string | Dealer or partner is accessing this API

try {
    $result = $api_instance->getFooters($subdealerid, $salespersonid, $footertype, $apiKey, $accountType);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling FootersApi->getFooters: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **subdealerid** | **string**|  |
 **salespersonid** | **string**|  |
 **footertype** | **string**|  |
 **apiKey** | **string**| API Key to access this dealer&#39;s resources. Value was returned when create_account api was called and dealer was created first time | [default to &lt;&lt;api_key&gt;&gt;]
 **accountType** | **string**| Dealer or partner is accessing this API | [default to &lt;&lt;account_type&gt;&gt;]

### Return type

[**\Braango\braangomodel\FootersOutput**](../Model/FootersOutput.md)

### Authorization

[auth_token](../../README.md#auth_token)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

