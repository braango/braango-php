# Braango\SmsApi

All URIs are relative to *https://api.braango.com/v2/braango*

Method | HTTP request | Description
------------- | ------------- | -------------
[**createSms**](SmsApi.md#createSms) | **POST** /personnel/SMS/{subdealerid}/{salespersonid} | Create SMS
[**deleteAllSms**](SmsApi.md#deleteAllSms) | **DELETE** /personnel/SMS/{subdealerid}/{salespersonid} | Delete SMS
[**deleteOnesms**](SmsApi.md#deleteOnesms) | **DELETE** /personnel/SMS/{subdealerid}/{salespersonid}/{number} | Delete One SMS
[**readSms**](SmsApi.md#readSms) | **GET** /personnel/SMS/{subdealerid}/{salespersonidstr} | Get SMS


# **createSms**
> \Braango\braangomodel\SmsOutput createSms($subdealerid, $salespersonid, $body)

Create SMS

This api call allows to add SMS number for a given _personnel_

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: auth_token
Braango\Configuration::getDefaultConfiguration()->setApiKey('auth_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Braango\Configuration::getDefaultConfiguration()->setApiKeyPrefix('auth_token', 'Bearer');

$api_instance = new Braango\Api\SmsApi();
$subdealerid = "subdealerid_example"; // string | id of _sub_account_
$salespersonid = "salespersonid_example"; // string | id of salesperson
$body = new \Braango\braangomodel\SmsInput(); // \Braango\braangomodel\SmsInput | 

try {
    $result = $api_instance->createSms($subdealerid, $salespersonid, $body);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling SmsApi->createSms: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **subdealerid** | **string**| id of _sub_account_ |
 **salespersonid** | **string**| id of salesperson |
 **body** | [**\Braango\braangomodel\SmsInput**](../Model/SmsInput.md)|  | [optional]

### Return type

[**\Braango\braangomodel\SmsOutput**](../Model/SmsOutput.md)

### Authorization

[auth_token](../../README.md#auth_token)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **deleteAllSms**
> \Braango\braangomodel\SmsOutput deleteAllSms($subdealerid, $salespersonid, $apiKey, $accountType)

Delete SMS

Delete all SMS numbers associated with this personnel

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: auth_token
Braango\Configuration::getDefaultConfiguration()->setApiKey('auth_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Braango\Configuration::getDefaultConfiguration()->setApiKeyPrefix('auth_token', 'Bearer');

$api_instance = new Braango\Api\SmsApi();
$subdealerid = "subdealerid_example"; // string | id of _sub_account_
$salespersonid = "salespersonid_example"; // string | id of salesperson
$apiKey = "<<api_key>>"; // string | API Key to access this dealer's resources. Value was returned when create_account api was called and dealer was created first time
$accountType = "<<account_type>>"; // string | Dealer or partner is accessing this API

try {
    $result = $api_instance->deleteAllSms($subdealerid, $salespersonid, $apiKey, $accountType);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling SmsApi->deleteAllSms: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **subdealerid** | **string**| id of _sub_account_ |
 **salespersonid** | **string**| id of salesperson |
 **apiKey** | **string**| API Key to access this dealer&#39;s resources. Value was returned when create_account api was called and dealer was created first time | [default to &lt;&lt;api_key&gt;&gt;]
 **accountType** | **string**| Dealer or partner is accessing this API | [default to &lt;&lt;account_type&gt;&gt;]

### Return type

[**\Braango\braangomodel\SmsOutput**](../Model/SmsOutput.md)

### Authorization

[auth_token](../../README.md#auth_token)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **deleteOnesms**
> \Braango\braangomodel\SmsOutput deleteOnesms($subdealerid, $salespersonid, $number, $apiKey, $accountType)

Delete One SMS

Delete SMS number resource provided in URL

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: auth_token
Braango\Configuration::getDefaultConfiguration()->setApiKey('auth_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Braango\Configuration::getDefaultConfiguration()->setApiKeyPrefix('auth_token', 'Bearer');

$api_instance = new Braango\Api\SmsApi();
$subdealerid = "subdealerid_example"; // string | id of _sub_account_
$salespersonid = "salespersonid_example"; // string | id of _sales_person_
$number = "number_example"; // string | sms number to be deleted
$apiKey = "<<api_key>>"; // string | API Key to access this dealer's resources. Value was returned when create_account api was called and dealer was created first time
$accountType = "<<account_type>>"; // string | Dealer or partner is accessing this API

try {
    $result = $api_instance->deleteOnesms($subdealerid, $salespersonid, $number, $apiKey, $accountType);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling SmsApi->deleteOnesms: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **subdealerid** | **string**| id of _sub_account_ |
 **salespersonid** | **string**| id of _sales_person_ |
 **number** | **string**| sms number to be deleted |
 **apiKey** | **string**| API Key to access this dealer&#39;s resources. Value was returned when create_account api was called and dealer was created first time | [default to &lt;&lt;api_key&gt;&gt;]
 **accountType** | **string**| Dealer or partner is accessing this API | [default to &lt;&lt;account_type&gt;&gt;]

### Return type

[**\Braango\braangomodel\SmsOutput**](../Model/SmsOutput.md)

### Authorization

[auth_token](../../README.md#auth_token)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **readSms**
> \Braango\braangomodel\SmsOutput readSms($subdealerid, $salespersonidstr, $apiKey, $accountType)

Get SMS

Get the details of the SMS configured for this _personnel_

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: auth_token
Braango\Configuration::getDefaultConfiguration()->setApiKey('auth_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Braango\Configuration::getDefaultConfiguration()->setApiKeyPrefix('auth_token', 'Bearer');

$api_instance = new Braango\Api\SmsApi();
$subdealerid = "subdealerid_example"; // string | _sub_dealer_ resource that owns this personnel resource
$salespersonidstr = "salespersonidstr_example"; // string | ID of personnel that owns this sub_resource
$apiKey = "<<api_key>>"; // string | API Key to access this dealer's resources. Value was returned when create_account api was called and dealer was created first time
$accountType = "<<account_type>>"; // string | Dealer or partner is accessing this API

try {
    $result = $api_instance->readSms($subdealerid, $salespersonidstr, $apiKey, $accountType);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling SmsApi->readSms: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **subdealerid** | **string**| _sub_dealer_ resource that owns this personnel resource |
 **salespersonidstr** | **string**| ID of personnel that owns this sub_resource |
 **apiKey** | **string**| API Key to access this dealer&#39;s resources. Value was returned when create_account api was called and dealer was created first time | [default to &lt;&lt;api_key&gt;&gt;]
 **accountType** | **string**| Dealer or partner is accessing this API | [default to &lt;&lt;account_type&gt;&gt;]

### Return type

[**\Braango\braangomodel\SmsOutput**](../Model/SmsOutput.md)

### Authorization

[auth_token](../../README.md#auth_token)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

