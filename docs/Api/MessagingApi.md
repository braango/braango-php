# Braango\MessagingApi

All URIs are relative to *https://api.braango.com/v2/braango*

Method | HTTP request | Description
------------- | ------------- | -------------
[**messageBlast**](MessagingApi.md#messageBlast) | **POST** /communications/messageblast | Message Blast


# **messageBlast**
> \Braango\braangomodel\MessageBlastOutputWrapper messageBlast($body)

Message Blast

This API allows partner or dealer to blast messages to one or multiple numbers at the same time. Each person can receive different message  The blasting number is seperate than regular braango number. At this moment that number will be created by Braango Support Team on behest of of partner. Eventually there will be simple API to create blasting number  Partner can choose to blast messages to anyone including dealers  Responses to these messages seeds the client into our database and are sent via REST api call to the partner or master account  Based on response, partner can then decide to bridge the client to particular _sub_dealer_ or _personnel_ within _sub_dealer_ using __dealerconnect__ api  The above described use case of **_late_binding_** mode of operation  Please refer to flow diagrams for various use case flows of this API

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: auth_token
Braango\Configuration::getDefaultConfiguration()->setApiKey('auth_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Braango\Configuration::getDefaultConfiguration()->setApiKeyPrefix('auth_token', 'Bearer');

$api_instance = new Braango\Api\MessagingApi();
$body = new \Braango\braangomodel\MessageBlastRequestInput(); // \Braango\braangomodel\MessageBlastRequestInput | 

try {
    $result = $api_instance->messageBlast($body);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling MessagingApi->messageBlast: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**\Braango\braangomodel\MessageBlastRequestInput**](../Model/MessageBlastRequestInput.md)|  | [optional]

### Return type

[**\Braango\braangomodel\MessageBlastOutputWrapper**](../Model/MessageBlastOutputWrapper.md)

### Authorization

[auth_token](../../README.md#auth_token)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

