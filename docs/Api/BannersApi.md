# Braango\BannersApi

All URIs are relative to *https://api.braango.com/v2/braango*

Method | HTTP request | Description
------------- | ------------- | -------------
[**createBanners**](BannersApi.md#createBanners) | **POST** /personnel/banners/{subdealerid}/{salespersonid} | Create banners
[**deleteBanners**](BannersApi.md#deleteBanners) | **DELETE** /personnel/banners/{subdealerid}/{salespersonid}/{bannertype} | Delete banners
[**getBanners**](BannersApi.md#getBanners) | **GET** /personnel/banners/{subdealerid}/{salespersonid}/{bannertype} | Get banners


# **createBanners**
> \Braango\braangomodel\BannersOutput createBanners($subdealerid, $salespersonid, $body)

Create banners

Create list of banners. All three types of banners can be specified here, client, dealer or supervisor

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: auth_token
Braango\Configuration::getDefaultConfiguration()->setApiKey('auth_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Braango\Configuration::getDefaultConfiguration()->setApiKeyPrefix('auth_token', 'Bearer');

$api_instance = new Braango\Api\BannersApi();
$subdealerid = "subdealerid_example"; // string | id of _sub_dealer_
$salespersonid = "salespersonid_example"; // string | id of _personnel_
$body = new \Braango\braangomodel\BannersInput(); // \Braango\braangomodel\BannersInput | 

try {
    $result = $api_instance->createBanners($subdealerid, $salespersonid, $body);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling BannersApi->createBanners: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **subdealerid** | **string**| id of _sub_dealer_ |
 **salespersonid** | **string**| id of _personnel_ |
 **body** | [**\Braango\braangomodel\BannersInput**](../Model/BannersInput.md)|  | [optional]

### Return type

[**\Braango\braangomodel\BannersOutput**](../Model/BannersOutput.md)

### Authorization

[auth_token](../../README.md#auth_token)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **deleteBanners**
> \Braango\braangomodel\BannersOutput deleteBanners($subdealerid, $salespersonid, $bannertype, $apiKey, $accountType)

Delete banners

Delete banners by specifying banner type

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: auth_token
Braango\Configuration::getDefaultConfiguration()->setApiKey('auth_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Braango\Configuration::getDefaultConfiguration()->setApiKeyPrefix('auth_token', 'Bearer');

$api_instance = new Braango\Api\BannersApi();
$subdealerid = "subdealerid_example"; // string | id of _sub_dealer_
$salespersonid = "salespersonid_example"; // string | id of _personnel_
$bannertype = "bannertype_example"; // string | Banner type - `client` , `dealer` , `supervisor`
$apiKey = "<<api_key>>"; // string | API Key to access this dealer's resources. Value was returned when create_account api was called and dealer was created first time
$accountType = "<<account_type>>"; // string | Dealer or partner is accessing this API

try {
    $result = $api_instance->deleteBanners($subdealerid, $salespersonid, $bannertype, $apiKey, $accountType);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling BannersApi->deleteBanners: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **subdealerid** | **string**| id of _sub_dealer_ |
 **salespersonid** | **string**| id of _personnel_ |
 **bannertype** | **string**| Banner type - &#x60;client&#x60; , &#x60;dealer&#x60; , &#x60;supervisor&#x60; |
 **apiKey** | **string**| API Key to access this dealer&#39;s resources. Value was returned when create_account api was called and dealer was created first time | [default to &lt;&lt;api_key&gt;&gt;]
 **accountType** | **string**| Dealer or partner is accessing this API | [default to &lt;&lt;account_type&gt;&gt;]

### Return type

[**\Braango\braangomodel\BannersOutput**](../Model/BannersOutput.md)

### Authorization

[auth_token](../../README.md#auth_token)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getBanners**
> \Braango\braangomodel\BannersOutput getBanners($subdealerid, $salespersonid, $bannertype, $apiKey, $accountType)

Get banners

Get the current list of banners for given banner_type as specified in the URL

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: auth_token
Braango\Configuration::getDefaultConfiguration()->setApiKey('auth_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Braango\Configuration::getDefaultConfiguration()->setApiKeyPrefix('auth_token', 'Bearer');

$api_instance = new Braango\Api\BannersApi();
$subdealerid = "subdealerid_example"; // string | id of _sub_dealer_
$salespersonid = "salespersonid_example"; // string | id of _personnel_
$bannertype = "bannertype_example"; // string | Banner type - `client` , `dealer` , `supervisor`
$apiKey = "<<api_key>>"; // string | API Key to access this dealer's resources. Value was returned when create_account api was called and dealer was created first time
$accountType = "<<account_type>>"; // string | Dealer or partner is accessing this API

try {
    $result = $api_instance->getBanners($subdealerid, $salespersonid, $bannertype, $apiKey, $accountType);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling BannersApi->getBanners: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **subdealerid** | **string**| id of _sub_dealer_ |
 **salespersonid** | **string**| id of _personnel_ |
 **bannertype** | **string**| Banner type - &#x60;client&#x60; , &#x60;dealer&#x60; , &#x60;supervisor&#x60; |
 **apiKey** | **string**| API Key to access this dealer&#39;s resources. Value was returned when create_account api was called and dealer was created first time | [default to &lt;&lt;api_key&gt;&gt;]
 **accountType** | **string**| Dealer or partner is accessing this API | [default to &lt;&lt;account_type&gt;&gt;]

### Return type

[**\Braango\braangomodel\BannersOutput**](../Model/BannersOutput.md)

### Authorization

[auth_token](../../README.md#auth_token)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

