# PersonnelOutput

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**smsLogin** | **bool** | Indicates whether this personnel can login via SMS. SMS enabled number can act as user id and 6 digit validation code that personnel received can be used in lieu of password. For this to happen, sms_login needs to be enabled. | [default to false]
**partnerDealer** | **bool** | Indicates whether this personnel is partner account or master account for franchise group | [default to false]
**dealerApiKey** | **string** | This the dealer&#39;s api key for partner dealer  that has been registered. This key is different from authentication token provided to partner. | 
**dealerId** | **string** | This is the dealer_id assigned to this dealer. For virtual dealer model, this ID will be that of partner dealer that is signing up dealers as sub-dealers. For franchise or hierachical business, this is the dealer ID assigned to the root account or partner dealer account. | 
**dealerPkId** | **int** | Integer representation of the dealer ID. This is unique within braango system and gets generated for partner dealer | 
**dealerName** | **string** | Name assigned while signing up this personnel. For _partner dealer_ (master account) this is assigned while creating an account | 
**salesPersonId** | **string** | UUID generated internally by &#x60;braango&#x60;  All sub-resources for this personnel are referenced via this ID | 
**personnelName** | **string** | Name assigned to this personnel | 
**subDealerMasterPersonnel** | **bool** | This flag indicates if this is a master account for this sub-dealer. This master account is hierarchically below partner dealer. Typically this will be an account held by manager at location or dealership. This account has some privileges over regular personnel accounts | 
**userName** | **string** | This is a user name created while signing this personnel up. Typically this user name can be used to log into the braango UI. However for whitelabel product, it is expected that this will be used for single signon with respect to dealer account on partner system. i.e. it is expected that partner will pass on the same user name that dealer has on its system to give seamless integration experience. | 
**enabled** | **bool** | For account that is active, this will be true. | 
**privileges** | **string** | This personnel can have privileges of dealer, subDealer or personnel. | 
**groups** | **string[]** | List of groups this personnel has subscribed to. | 
**emailList** | **string[]** | List of email address where personnel can receive TEXT messages from clients | 
**smsList** | **string[]** | List of SMS Enabled phone number where TEXT messages are bridged.  The numbers in this list are in  format. Currently only US and Canada numbers are supported. | 
**voiceList** | **string[]** | List of VOICE numbers where voice calls need to be bridged  Currently only US and Canada numbers are supported | 
**crmEmailList** | **string[]** | List of email addresses where leads will be sent in ADF XML format. Note either leads are sent to ADF XML enabled CRMS or other CRMS (base, zoho and pipe) not both | 
**dealerBanners** | **string[]** | List of banners that this personnel will see when client sends TEXT. Braango will randomly pick one from the list | 
**clientBanners** | **string[]** | Banner message that client will see when this personnel communications over SMS. Braango will randomly choose one from the list | 
**supervisorBanners** | **string[]** | When personnel is acting as supervisor, this will be the banner message that personnel will see. Braango will choose one from the list randomly. | 
**clientFooters** | **string[]** | Footer message that client sees when this personnel communicates over SMS. Braango will randomly choose from one | 
**dealerFooters** | **string[]** | Footer message that this personnel will see when client communicates. Braango will randomly choose one from this list. | 
**supervisorFooters** | **string[]** | Footer message that this personnel will see  when acting as supervisor when client communicates. Braango will randomly choose one from this list. | 
**groupsSupervising** | **string[]** | List of groups that this personnel is supervising. This will be used in future, at present does nothing | 
**supervisors** | [**\Braango\braangomodel\PersonnelOutputSupervisors[]**](PersonnelOutputSupervisors.md) | This hash map of list of of supervisors per group that this personnel has subscribed and assigned the supervisor.   &#x60;&#x60;&#x60;java    Map&lt;String,List&lt;String&gt; supervisors | 
**subordinates** | [**\Braango\braangomodel\PersonnelOutputSubordinates[]**](PersonnelOutputSubordinates.md) | This hash map of list of of subordinates per group that this personnel has subscribed and is monitoring other personnel in that group.   &#x60;&#x60;&#x60;java    Map&lt;String,List&lt;String&gt; subOrdinates | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


