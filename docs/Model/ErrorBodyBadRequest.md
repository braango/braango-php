# ErrorBodyBadRequest

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**status** | **string** | Indicates if it is ERROR or WARNING | 
**message** | **string** | Human readable ERROR or WARNING message | 
**messageCode** | **string** | Machine parse able ERROR or WARNING code | 
**data** | **object** | Error or Warning payload | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


