# VoiceOutputBodyData

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**voiceList** | [**\Braango\braangomodel\VoiceOutputBodyDataVoiceList[]**](VoiceOutputBodyDataVoiceList.md) | Array of voice numbers for this personnel | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


