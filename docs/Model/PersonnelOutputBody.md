# PersonnelOutputBody

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**data** | [**\Braango\braangomodel\PersonnelOutput**](PersonnelOutput.md) |  | [optional] 
**status** | **string** | Indicates if it is SUCESS or WARNING | [optional] 
**message** | **string** | Human readable string of possible warning | [optional] 
**messageCode** | **string** | parse able string in case of warning | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


