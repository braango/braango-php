# AddClientOutputWrapperBody

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**status** | **string** |  | [optional] 
**message** | **string** |  | [optional] 
**messageCode** | **string** |  | [optional] 
**data** | [**\Braango\braangomodel\AddClientOutputWrapperBodyData**](AddClientOutputWrapperBodyData.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


