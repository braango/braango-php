# BraangoNumberCreateInputBody

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**allPersonnel** | **bool** | This value indicates if the braango number to be created is applicable to all personnel that have subscribed to the group for which braango number is being created. __Default__ is all __true__. | 
**fakeBraangoNumber** | **bool** | Set this to 1 when developing application using this API. This will create a mock non functional braango number, but will help test out braango number. | 
**salesPersonIdMulti** | **string[]** | List of sales personnel that need to be added . Only if _all_personnel_ flag is false. _all_personnel_ flag overrides this | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


