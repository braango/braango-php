# SupervisorOutputBodyData

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**supervisors** | [**\Braango\braangomodel\SupervisorOutputBodyDataSupervisors[]**](SupervisorOutputBodyDataSupervisors.md) | List of supervisors | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


