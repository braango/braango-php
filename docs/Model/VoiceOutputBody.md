# VoiceOutputBody

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**status** | **string** | Either SUCCESS or ERROR or WARNING | 
**messageCode** | **string** | Only valid and present in case of ERROR or WARNING. Note valid value of NULL is permitted. | [optional] 
**message** | **string** | Only valid and present in case of ERROR or WARNING | [optional] 
**data** | [**\Braango\braangomodel\VoiceOutputBodyData**](VoiceOutputBodyData.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


