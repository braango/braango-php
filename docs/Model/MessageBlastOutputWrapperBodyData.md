# MessageBlastOutputWrapperBodyData

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**numbers** | [**\Braango\braangomodel\MessageBlastOutputWrapperBodyDataNumbers[]**](MessageBlastOutputWrapperBodyDataNumbers.md) | Return response of request | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


