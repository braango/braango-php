# FootersOutputBodyData

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**dealerFooters** | **string[]** | List of current dealer footers | [optional] 
**clientFooters** | **string[]** | List of current client footers | [optional] 
**supervisorFooters** | **string[]** | List of current supervisor footers | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


