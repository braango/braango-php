# SupervisorInputBody

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**supervisor** | **string** | sales person id of _personnel_ that needs to be the supervisor. This is created when calling create personnel (regular) api call | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


