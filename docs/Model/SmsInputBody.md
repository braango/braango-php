# SmsInputBody

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**smsNumber** | **string** | Cell number where TEXT messages can be received or sent from | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


