# DealerConnectRequestInput

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**header** | [**\Braango\braangomodel\RequestHeader**](RequestHeader.md) |  | 
**body** | [**\Braango\braangomodel\DealerConnect**](DealerConnect.md) |  | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


