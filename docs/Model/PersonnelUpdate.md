# PersonnelUpdate

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**smsLogin** | **bool** | Indicates whether this personnel can login via SMS. SMS enabled number can act as user id and 6 digit validation code that personnel received can be used in lieu of password. For this to happen, sms_login needs to be enabled. | [optional] [default to false]
**password** | **string** | Password will be encrypted with SHA-25 and base64 encoded and stored internally within the braango system. | [optional] 
**status** | **string** | Values are Active, Inactive, Vacation | [optional] 
**dealerBanners** | **string[]** | List of banners that this personnel will see when client sends TEXT. Braango will randomly pick one from the list | [optional] 
**clientBanners** | **string[]** | Banner message that client will see when this personnel communications over SMS. Braango will randomly choose one from the list | [optional] 
**supervisorBanners** | **string[]** | When personnel is acting as supervisor, this will be the banner message that personnel will see. Braango will choose one from the list randomly. | [optional] 
**clientFooters** | **string[]** | Footer message that client sees when this personnel communicates over SMS. Braango will randomly choose from one | [optional] 
**dealerFooters** | **string[]** | Footer message that this personnel will see when client communicates. Braango will randomly choose one from this list. | [optional] 
**supervisorFooters** | **string[]** | Footer message that this personnel will see  when acting as supervisor when client communicates. Braango will randomly choose one from this list. | [optional] 
**smsNumber** | **string** | Cell or SMS number of the personnel | [optional] 
**phoneNumber** | **string** | Voice number of the personnel | [optional] 
**email** | **string** | Email that can be used to receive and send text messages | [optional] 
**typeAdfCrm** | **bool** | If true, email is of ADF compatible CRM | [optional] 
**group** | **string** | Group that personnel belongs or is attempting to subscribe to | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


