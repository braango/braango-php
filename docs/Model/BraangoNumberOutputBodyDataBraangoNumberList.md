# BraangoNumberOutputBodyDataBraangoNumberList

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**braangoNumberId** | **string** | Internal ID for braango number object | [optional] 
**salesPersonId** | **string** | personnel owning this braango number | [optional] 
**group** | **string** | Group associated with this braango number | [optional] 
**salesPersonIdMulti** | **string[]** | List of personnel associated with this braango number | [optional] 
**braangoNumbers** | **string[]** | List of braango numbers associated with this group and this object | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


