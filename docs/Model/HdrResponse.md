# HdrResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **string** | This value is reflection for POST and PUT method&#39;s id value in header if specified. If value is null in those requests, this value will be always be current session id. For GET and DELETE it will always be session id. This value is not cached into the braango server | [optional] 
**isnRequestId** | **string** |  | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


