# VoiceOutputBodyDataVoiceList

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**phoneNumber** | **string** | Actual number where voice calls can be received | 
**enable** | **bool** | If true voice calls can be received | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


