# SupervisorOutputBodyDataSupervisors

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**group** | **string** | Group that these supervisors belong to | [optional] 
**supervisors** | **string[]** | Actual list of supervisors for the given group | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


