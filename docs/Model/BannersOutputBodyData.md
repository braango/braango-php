# BannersOutputBodyData

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**dealerBanners** | **string[]** | current dealer banners | [optional] 
**clientBanners** | **string[]** | current client banners | [optional] 
**supervisorBanners** | **string[]** | current supervisor banners | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


