# PersonnelOutputListWrapperBody

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**data** | [**\Braango\braangomodel\PersonnelOutput[]**](PersonnelOutput.md) |  | 
**status** | **string** | Indicates if response is SUCCES,ERROR or WARNING | 
**message** | **string** | Human readable ERROR or WARNING message | [optional] 
**messageCode** | **string** | Machine parse able ERROR or WARNING code | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


