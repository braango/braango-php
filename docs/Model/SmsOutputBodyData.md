# SmsOutputBodyData

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**smsList** | [**\Braango\braangomodel\SmsOutputBodyDataSmsList[]**](SmsOutputBodyDataSmsList.md) | List of SMS enabled numbers for this personnel | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


