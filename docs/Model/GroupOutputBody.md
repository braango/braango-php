# GroupOutputBody

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**status** | **string** | If operation is success, this will be SUCCESS else ERROR or WARNING | [default to '']
**message** | **string** | Valid if stat is ERROR. Human readable error message | [optional] 
**messageCode** | **string** | Valid only if status ERROR, returns ERROR-CODE | [optional] 
**data** | [**\Braango\braangomodel\GroupOutputBodyData**](GroupOutputBodyData.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


