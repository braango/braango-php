# MessageBlast

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**numbers** | [**\Braango\braangomodel\MessageBlastNumbers[]**](MessageBlastNumbers.md) | Array of numbers that need to be blasted. Each element of array is unique number, with its own message | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


