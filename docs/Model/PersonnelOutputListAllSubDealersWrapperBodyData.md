# PersonnelOutputListAllSubDealersWrapperBodyData

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**personnel** | [**\Braango\braangomodel\PersonnelOutput[]**](PersonnelOutput.md) |  | [optional] 
**subDealerId** | **string** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


