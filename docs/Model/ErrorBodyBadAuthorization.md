# ErrorBodyBadAuthorization

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**status** | **string** | ERROR | 
**message** | **string** | Human readable ERROR indicating bad authorization | 
**messageCode** | **string** | Machine parse able ERROR code for authentication and/or authorization error | 
**data** | **object** | Error payload | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


