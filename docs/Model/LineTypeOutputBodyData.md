# LineTypeOutputBodyData

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**lineType** | **string** | Line type : Either mobile, landline, VOIP etc. | [optional] 
**valid** | **bool** |  | [optional] 
**phoneNumber** | **string** | Number used for determining the line type | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


