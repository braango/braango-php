# BraangoNumberInputBody

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**allPersonnel** | **bool** | This value indicates if the braango number to be created is applicable to all personnel that have subscribed to the group for which braango number is being created. Default is all true. | 
**fakeBraangoNumber** | **bool** | Set this to 1 when developing application using this API. This will create a mock non functional braango number, but will help test out braango number. | 
**add** | **bool** | This flag indicates whether list of salesPerson provided along with this call is to be added to or deleted from the braangonumber resource that will be created/updated/deleted | 
**salesPersonIdMulti** | **string[]** | List of sales personnel that need to be added or removed from this braango number resource | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


