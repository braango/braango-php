# FootersOutput

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**header** | [**\Braango\braangomodel\ResponseHeader**](ResponseHeader.md) |  | 
**body** | [**\Braango\braangomodel\FootersOutputBody**](FootersOutputBody.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


