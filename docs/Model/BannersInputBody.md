# BannersInputBody

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**dealerBanners** | **string[]** | Banners that dealer will see on TEXT messages. Braango will randomly choose from one for every message received | [optional] 
**clientBanners** | **string[]** | Banners that client will see on TEXT messages. Braango will randomly choose from one for every message received | [optional] 
**supervisorBanners** | **string[]** | Banners that supervisor will see on TEXT messages. Braango will randomly choose from one for every message received | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


