# Header

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**apiKey** | **string** | API Key for the dealer returned when create_account was called. | [default to '<<api_key>>']
**accountType** | **string** | Account type should be partner for channel partners and integrators ; dealer for dealer access | [default to '<<account_type>>']

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


