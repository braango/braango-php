# MessageBlastOutputWrapperBody

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**status** | **string** | SUCCESS or ERROR . If ERROR, message-code will indicate class of error and message will describe the error | 
**message** | **string** | Human readable ERROR or WARNING message | [optional] 
**messageCode** | **string** | Machine parse able ERROR or WARNING code | [optional] 
**data** | [**\Braango\braangomodel\MessageBlastOutputWrapperBodyData**](MessageBlastOutputWrapperBodyData.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


