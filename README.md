# Braango
Braango API for partners to onboard dealers , configure and allow for send messages on behalf of dealers

[The raw API is defined here](https://braango.api-docs.io/v2)

This PHP package is automatically generated by the [Swagger Codegen](https://github.com/swagger-api/swagger-codegen) project:

- API version: 2.0
- Package version: 1.0.0
- Build package: io.swagger.codegen.languages.PhpClientCodegen

## Requirements

PHP 5.4.0 and later

## Installation & Usage
### Composer

To install the bindings via [Composer](http://getcomposer.org/), add the following to `composer.json`:

```
{
  "repositories": [
    {
      "type": "git",
      "url": "https://github.com/braango/braango.git"
    }
  ],
  "require": {
    "braango/braango": "*@dev"
  }
}
```

Then run `composer install`

### Manual Installation

Download the files and include `autoload.php`:

```php
    require_once('/path/to/GeneratedBraango/autoload.php');
```

## Tests

To run the unit tests:

```
composer install
./vendor/bin/phpunit
```

## Getting Started

Please follow the [installation procedure](#installation--usage) and then run the following:

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: auth_token
Braango\Configuration::getDefaultConfiguration()->setApiKey('auth_token', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// Braango\Configuration::getDefaultConfiguration()->setApiKeyPrefix('auth_token', 'Bearer');

$api_instance = new Braango\Api\AccountsApi();
$body = new \Braango\braangomodel\AccountCreateRequestInput(); // \Braango\braangomodel\AccountCreateRequestInput | 

try {
    $result = $api_instance->createPartnerDealer($body);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AccountsApi->createPartnerDealer: ', $e->getMessage(), PHP_EOL;
}

?>
```

## Documentation for API Endpoints

All URIs are relative to *https://api.braango.com/v2/braango*

## Documentation For Authorization


## auth_token

- **Type**: API key
- **API key parameter name**: auth_token
- **Location**: URL query string


## Author




